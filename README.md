# STAC Device

This project contains the code for the STAC (Safe Tool Access Control) device.

## Getting Started

### Setup STAC Device

This project's main program will only run on a Raspberry Pi, upon which the STAC device is based. For instructions for setting up the physical device, installing required dependencies, and configuring environment variables, see the [STAC Device Setup](https://gitlab.com/safe-shop-wms/product-backlog/-/wikis/Setup-Guide/STAC-Device-Setup) wiki.

### Setup for Local Development

While the main program cannot be run on a computer other than a Raspberry Pi, you can work on the program and test certain components on any computer.

If needed, reference the [STAC Device Setup](https://gitlab.com/safe-shop-wms/product-backlog/-/wikis/Setup-Guide/STAC-Device-Setup) to ensure you have the appropriate environment files filled out and available in your development environment.

The STAC device application is written in Python. Install the following if not already available and ensure they are on your path:

* python3 with pip

Install all required Python packages by running `pip3 install -r requirements.txt`.

## Run It

From the main project directory on a Raspberry Pi, run the main program: `python3 stac_device/main.py`.

Note: The setup script utilized in the [STAC Device Setup](https://gitlab.com/safe-shop-wms/product-backlog/-/wikis/Setup-Guide/STAC-Device-Setup) configures the main.py script to run automatically when the Pi boots up. It can take 1 - 1.5 minutes after turning on the Pi for the program to fully start, indicated by the menu appearing on the OLED screen.

### Optional Logging

The level of logging is configurable via the 'env_constants.py' file. The STAC program's logs are logged at INFO or higher, while dependencies may log at DEBUG level and up. Logs are displayed in the console.

## Test It

This project uses the `pytest` module for testing. Tests are not meant to be run on the Raspberry Pi but are runnable on any computer.

### Run Tests

`python3 -m pytest`

### Analyze Test Coverage

Run tests with coverage analysis:

`python3 -m coverage run --source=./stac_device --omit=./stac_device/env_constants.py -m pytest`

Then create test coverage report:

`python3 -m coverage report`

# Service to handle interaction with the RFID Reader

import env_constants as env
import time
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import logging

# Initialize Utilities
logging.basicConfig(level=env.LOGGING_LEVEL)
rfidLogger = logging.getLogger("rfid_service")

class RfidService:

    def __init__(self):
        self.reader = SimpleMFRC522()

    # Checks for an RFID card once every second.
    # Once card is detected, returns the card ID as a string.
    # If timeout is reached without card detected, raises exception.
    def readCardIdWithTimeout(self, timeout):
        count = 0
        while True:
            if count == timeout:
                raise Exception("Timeout exceeded. No card detected.")
            id = self.reader.read_id_no_block()
            if id:
                return str(id)
            count+=1
            time.sleep(1)

    # Every env.CARD_CHECK_FREQUENCY seconds, attempt to read card up to 5 times.
    # If card not read, exception bubbles up.
    # If card read, but id does not match original card, exception is raised.
    def repeatCardReadAtInterval(self, originalCardId):
        while(True):
            rfidLogger.info("Checking")
            nextCardId = self.readCardIdWithTimeout(5)
            if nextCardId != originalCardId:
                raise Exception('Different user detected.')
            time.sleep(env.CARD_CHECK_FREQUENCY) 

    # Run at program exit
    def cleanup(self):
        GPIO.cleanup()

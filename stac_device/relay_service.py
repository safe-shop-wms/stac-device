# Service to handle interaction with the Relay

import env_constants as env
import gpiozero

class RelayService:

    def __init__(self):
        # Relay power is triggered by high output and should be initially off.
        self.relay = gpiozero.OutputDevice(env.RELAY_PIN, active_high=True, initial_value=False)

    def on(self):
        self.relay.on()

    def off(self):
        self.relay.off()

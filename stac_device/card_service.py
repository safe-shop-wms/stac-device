# Service to handle interaction with the Card API

import env_constants as env

class CardService:
    CARD_ENDPOINT=env.AWS_API_URL+'/card'
    
    # Submits userId and cardId to CARD_ENDPOINT
    # Raises an exception if response.status_code != 200.
    def updateUserCard(self, apiUtils, userId, cardId):
        requestBody = {
            "userId": userId,
            "cardId": cardId
        }

        response = apiUtils.put(self.CARD_ENDPOINT, requestBody)

        if response.status_code != 200:
            raise Exception(f'updateUserCard API Error: {response.status_code}: {response.json()}')
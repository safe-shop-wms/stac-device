# Safe Shop STAC Device Script

# Configuration/Environment Data
import env_constants as env

# Services
from api_utils import ApiUtils
from auth_service import AuthService
from card_service import CardService
from config_service import ConfigService
from display_service import DisplayService
from keypad_service import KeypadService
from led_service import LedService
from rfid_service import RfidService
from relay_service import RelayService

### Utility Imports (Cleanup, etc.)
import time
import atexit
import logging

# Initialize Services
display = DisplayService()
apiUtils = ApiUtils()
apiUtils.getCredentials()
auth = AuthService()
cardService = CardService()
config = ConfigService()
keypad = KeypadService()
led = LedService()
reader = RfidService()
relay = RelayService()

# Initialize Utilities
logging.basicConfig(level=env.LOGGING_LEVEL)
mainLogger = logging.getLogger("main")

### Cleanup functions
def cleanup():
    display.clearDisplay()
    reader.cleanup()

# Run cleanup on exit
atexit.register(cleanup)

# Main program loop
while True:

    led.green()

    ### Welcome Prompt with Menu ###
    display.displayMessage("STAC",
        "1) Use Tool", "2) Admin Menu", "")
    time.sleep(2)

    mainMenuChoice = keypad.readSingleDigit(display, led, "Menu choice:")

    # 1) Use Tool
    if mainMenuChoice == "1":

        display.displayMessage("Scan your card to use the tool.", "", "", "")

        # Attempt to read card.
        # If no card detected after 10 seconds, continue at start of main loop.
        try:
            cardId = reader.readCardIdWithTimeout(10)
        except Exception as error:
            led.red()
            mainLogger.info(error)
            display.displayMessage("Timeout exceeded.", "No card detected.","","")
            time.sleep(2)
            continue

        # Print status message
        display.displayMessage("Card scan successful.",
            "Enter PIN:", "","")

        userPin = keypad.readDigits(display, led, "Enter PIN:", env.PIN_MAX_LENGTH)

        # Print status message
        display.displayMessage("Authenticating User...", "","", "")

        # Attempt to authorize user to use tool
        try:
            authorized = auth.checkToolAccess(apiUtils, cardId, userPin, config.getToolId())
            if authorized == True:
                display.displayMessage("Access authorized.","","Power on.","")

                led.green()

                # Turn relay on
                relay.on()

                # Check for same card at intervals.
                # Logout if check raises an exception.
                try:
                    reader.repeatCardReadAtInterval(cardId)
                except Exception as error:

                    led.red()

                    #Turn relay off
                    relay.off()
                    mainLogger.info(error)
                    display.displayMessage("User logged out.","","Power off.","")
                    time.sleep(5)

            else:
                led.red()
                display.displayMessage("Access denied.","","Remove card.","")
                time.sleep(5)
        except Exception as error:
            led.red()
            mainLogger.error(error)
            display.displayMessage("API Error", "Remove card.", "Try later.","")
            time.sleep(5)

    # 2) Admin Menu
    elif mainMenuChoice == "2":
        display.displayMessage("Admin Pin:", "", "", "")
        time.sleep(1)
        adminPin = keypad.readDigits(display, led, "Admin PIN:", env.PIN_MAX_LENGTH)

        if adminPin != config.getAdminPin():
            led.red()
            display.displayMessage("Admin PIN Incorrect", "", "", "")
            time.sleep(3)
        else:
            display.displayMessage("Admin Menu", "1) Reset Tool ID", "2) Change Admin PIN", "3) Update Card ID")

            adminMenuChoice = keypad.readSingleDigit(display, led, "Menu choice:")

            # 1) Reset Tool ID
            if adminMenuChoice == '1':
                display.displayMessage("Reset Tool ID", "Use database ID", "Updated Tool ID:", "")
                time.sleep(2)
                newToolId = keypad.readDigits(display, led, "Updated Tool ID:", 15)
                config.setToolId(newToolId)
                display.displayMessage("Tool ID Reset", "", "", "")
                time.sleep(2)

            # 2) Change Admin PIN
            elif adminMenuChoice == '2':
                display.displayMessage("Change Admin PIN", "Max Length: ", str(env.PIN_MAX_LENGTH), "New Admin PIN:")
                time.sleep(2)
                newAdminPin = keypad.readDigits(display, led, "New Admin PIN:", env.PIN_MAX_LENGTH)
                config.setAdminPin(newAdminPin)
                display.displayMessage("Admin PIN Changed", "", "", "")
                time.sleep(2)

            # 3) Update User Card ID
            elif adminMenuChoice == '3':
                display.displayMessage("Update Card ID", "Input User ID:", "", "")
                time.sleep(2)
                userId = keypad.readDigits(display, led, "Input User ID:", 15)
                display.displayMessage("Scan User's Card.", "", "", "")
                
                # Attempt to read card.
                # If no card detected after 10 seconds, continue at start of main loop.
                try:
                    cardId = reader.readCardIdWithTimeout(10)
                except Exception as error:
                    led.red()
                    mainLogger.info(error)
                    display.displayMessage("Timeout exceeded.", "No card detected.","","")
                    time.sleep(2)
                    continue

                display.displayMessage("Scan successful.", "Submitting update...", "", "")

                try:
                    cardService.updateUserCard(apiUtils, userId, cardId)
                    led.green()
                    display.displayMessage("Update successful.", "", "", "")
                    time.sleep(2)
                except Exception as error:
                    led.red()
                    mainLogger.error(error)
                    display.displayMessage("API Error", "Update failed.", "Try later.","")
                    time.sleep(5)             

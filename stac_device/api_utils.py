# Utility to handle API credentials requests

import boto3
import env_constants as env
import requests
from requests_aws4auth import AWS4Auth

class ApiUtils:
    API_ACCESS_CREDENTIALS=''

    def __init__(self):
        self.getCredentials()

    # Loads credentials
    # Call once at class initialization.
    # Call again as necessary when unauthorized response recieved from API.
    def getCredentials(self):
        cognitoClient = boto3.client('cognito-identity', env.AWS_REGION)
        idResponse = cognitoClient.get_id(
            AccountId=env.AWS_ACCOUNT_ID,
            IdentityPoolId=env.AWS_STAC_DEVICE_IDENTITY_POOL_ID
        )
        identityId = idResponse['IdentityId']
        credentialsResponse = cognitoClient.get_credentials_for_identity(
            IdentityId=identityId
        )
        accessKeyId = credentialsResponse['Credentials']['AccessKeyId']
        secretKey = credentialsResponse['Credentials']['SecretKey']
        sessionToken = credentialsResponse['Credentials']['SessionToken']
        self.API_ACCESS_CREDENTIALS=AWS4Auth(accessKeyId, secretKey, env.AWS_REGION, 'execute-api', session_token=sessionToken)

    # Post request to path with requestBody
    # Request new token and repeat request once if first response is 403.
    def post(self, path, requestBody):
        response = requests.post(path, requestBody, auth=self.API_ACCESS_CREDENTIALS)

        # To handle token timeout causing a 403, request new token, then repeat auth request
        if response.status_code == 403:
            self.getCredentials()
            return requests.post(path, requestBody, auth=self.API_ACCESS_CREDENTIALS)
        else:
            return response

    # Put request to path with requestBody
    # Request new token and repeat request once if first response is 403.
    def put(self, path, requestBody):
        response = requests.put(path, requestBody, auth=self.API_ACCESS_CREDENTIALS)

        # To handle token timeout causing a 403, request new token, then repeat auth request
        if response.status_code == 403:
            self.getCredentials()
            return requests.put(path, requestBody, auth=self.API_ACCESS_CREDENTIALS)
        else:
            return response


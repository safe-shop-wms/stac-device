# Service to handle interaction with the Users API

import requests
import env_constants as env
import json

class UserService:
    USER_ENDPOINT=env.API_BASE_URL+'/users'

    # Submits cardId for userId to USER_ENDPOINT
    # Raises an exeption if response.status_code != 200
    def registerUserCardId(self, userId, cardId):
        requestData = {
            "UpdateExpression": "SET card_id=:cardId",
            "ExpressionAttributeValues": {
                ":cardId": {
                    "S": cardId
                }
            },
            "Key": {
                "id": {
                    "N": userId
                }
            },
            "TableName": "users"
        }
        
        response = requests.put(self.USER_ENDPOINT, data = json.dumps(requestData))
        
        if response.status_code != 200:
            raise Exception(f'registerUserCardId API Error: {response.status_code}: {response.json()}')
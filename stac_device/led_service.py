# Service to handle interaction with the LED

# Imports
from gpiozero import RGBLED

class LedService:

    # Setup the LED
    def __init__(self):
        # GPIO pin numbers
        self.led = RGBLED(red=18, green=14, blue=15)

    def off(self):
        self.led.color = (0, 0, 0)

    def red(self):
        self.led.color = (1, 0, 0)

    def green(self):
        self.led.color = (0, 1, 0)

    def blue(self):
        self.led.color = (0, 0, 1)

    def yellow(self):
        self.led.color = (1, 1, 0)
    
# Service to handle interaction with the environment variables
# contained in env_variables.ini

import configparser
import os

class ConfigService:

    # Assumes 'stac-device' was cloned into pi user's '~' directory.
    ENV_VARIABLES_FILE_PATH='/home/pi/stac-device/stac_device/env_variables.ini'

    # Read initial env variables from config file
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.ENV_VARIABLES_FILE_PATH) 
        self.toolId = self.config['Device-Tool Config']['TOOL_ID']
        self.adminPin = self.config['Device Access Control']['ADMIN_PIN']

    # Return toolId in memory (to account for changes during program run)
    def getToolId(self):
        return self.toolId

    # Set the newToolId in memory and ini file
    def setToolId(self, newToolId):
        self.saveUpdatedVariableValue('TOOL_ID', '\d+', newToolId)
        self.toolId = newToolId

    # Return adminPin in memory (to account for changes during program run)
    def getAdminPin(self):
        return self.adminPin

    # Set the newAdminPin in memory and ini file
    def setAdminPin(self, newAdminPin):
        self.saveUpdatedVariableValue('ADMIN_PIN', '\d+', newAdminPin)
        self.adminPin = newAdminPin

    # Write updated value for the specified existing key with the given valueType 
    def saveUpdatedVariableValue(self, key, valueType, value):
        updateCommand = "perl -i -pe 's/^(" + key + "=)(" + valueType + ")$/$1." + value + "/e' " + self.ENV_VARIABLES_FILE_PATH
        os.system(updateCommand)

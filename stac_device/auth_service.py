# Service to handle interaction with the Auth API

import env_constants as env

class AuthService:
    AUTH_ENDPOINT=env.AWS_API_URL+'/auth'
    
    # Submits cardId, userPin, and toolID to AUTH_ENDPOINT
    # Returns true if API response is true.
    # Otherwise returns false.
    # Raises an exception if response.status_code != 200.
    def checkToolAccess(self, apiUtils, cardId, userPin, toolId):
        requestBody = {
            "cardId": cardId,
            "userPin": userPin,
            "toolId": toolId
        }

        response = apiUtils.post(self.AUTH_ENDPOINT, requestBody)

        if response.status_code == 200:
            responseBody = response.json()
            authorized = responseBody['Authorized']
            if authorized == True:
                return True
            else:
                return False      
        else:
            raise Exception(f'checkToolAccess API Error: {response.status_code}: {response.json()}')
# Service to handle interaction with the keypad

import adafruit_matrixkeypad
import board
import digitalio
import env_constants as env
import time

class KeypadService:

    # Setup the keypad
    def __init__(self):
        # Correlate pins with keypad rows and columns
        cols = [digitalio.DigitalInOut(x) for x in (board.D26, board.D20, board.D21)]
        rows = [digitalio.DigitalInOut(x) for x in (board.D5, board.D6, board.D13, board.D19)]
        keys = ((1, 2, 3),
            (4, 5, 6),
            (7, 8, 9),
            ('*', 0, '#'))

        # Initialize the matrix
        self.keypad = adafruit_matrixkeypad.Matrix_Keypad(rows, cols, keys)

    # Read in one digit from user and return immediately.
    def readSingleDigit(self, displayRef, ledRef, prompt):

        # Read key presses
        while True:

            ledRef.green()
            
            # Read pressed keys
            pressedKeys = self.keypad.pressed_keys

            # Evaluate first (if any) key read
            if pressedKeys:
                nextKey = str(pressedKeys[0])
                if nextKey == '*' or nextKey == '#':
                    displayRef.displayMessage(prompt, "Number required.", "Try again.", "")
                    ledRef.red()
                else:
                    return str(nextKey)
            time.sleep(0.2)

    # Read in up to maxDigits from user, displaying them as they are input along with prompt.
    # Return user input as string when user indicates entry complete.
    def readDigits(self, displayRef, ledRef, prompt, maxDigits):

        userInput = ""

        # Read key presses
        while True:

            ledRef.green()

            # If input is longer than max length, display error and reset input.
            if len(userInput) > maxDigits:
                ledRef.red()
                userInput = ""
                displayRef.displayMessage(prompt, "Max length exceeded.", "Try again.", "")
            
            # Read pressed keys
            pressedKeys = self.keypad.pressed_keys

            # Evaluate first (if any) key read
            if pressedKeys:
                nextKey = str(pressedKeys[0])
                if nextKey == '*' and len(userInput) > 0:
                    userInput = ""
                    displayRef.displayMessage(prompt, "Input cleared.", "Try again.", "")
                elif nextKey == '#' and len(userInput) > 0:
                    return str(userInput)
                elif nextKey != '#' and nextKey != '*':
                    userInput += nextKey
                    displayRef.displayMessage(prompt, userInput, "Press * to clear.", "Press # to submit.")
                # elif nextKey == '#' or '*' and len(userInput) == 0, that input is ignored.
            time.sleep(0.2)


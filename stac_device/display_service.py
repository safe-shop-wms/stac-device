# Service to handle interaction with the PiOLED

from board import SCL, SDA
import busio
from PIL import Image, ImageDraw, ImageFont
import adafruit_ssd1306

class DisplayService:

    # Run display setup functions
    def __init__(self):
        # Create the I2C interface.
        self.i2c = busio.I2C(SCL, SDA)

        # Create the SSD1306 OLED class with display pixel width and height specified.
        self.disp = adafruit_ssd1306.SSD1306_I2C(128, 32, self.i2c)

        self.clearDisplay()

        # Create blank image for drawing.
        # Make sure to create image with mode '1' for 1-bit color.
        self.width = self.disp.width
        self.height = self.disp.height
        self.image = Image.new("1", (self.width, self.height))

        # Get drawing object to draw on image.
        self.draw = ImageDraw.Draw(self.image)

        # Define constants to allow easy drawing.
        self.padding = -2
        self.top = self.padding
        self.bottom = self.height - self.padding
        # Move left to right keeping track of the current x position for drawing.
        self.x = 0

        # Load default font.
        self.font = ImageFont.load_default()
            
    # Displays the given text on each of the four lines of the screen.
    # For lines that are not needed, pass in an empty string "".
    def displayMessage(self, line1, line2, line3, line4):
        # Draw a black filled box to clear the image.
        self.draw.rectangle((0, 0, self.width, self.height), outline=0, fill=0)
        self.draw.text((self.x, self.top + 0), line1, font=self.font, fill=255)
        self.draw.text((self.x, self.top + 8), line2, font=self.font, fill=255)
        self.draw.text((self.x, self.top + 16), line3, font=self.font, fill=255)
        self.draw.text((self.x, self.top + 25), line4, font=self.font, fill=255)

        # Display message.
        self.disp.image(self.image)
        self.disp.show()

    # Call this function on program exit to clear display
    def clearDisplay(self):
        self.disp.fill(0)
        self.disp.show()

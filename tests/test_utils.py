# Utility Classes and Functions for Tests

# MockResponse includes a 'status_code' and responseBody accessible via 'json()'
class MockResponse:

    #Constructor
    def __init__(self, statusCode, responseBody):
        self.status_code = statusCode
        self.response_body = responseBody

    def json(self):
        return self.response_body
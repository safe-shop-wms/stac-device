# Unit Tests for the User Service
# Needs update

import pytest
from user_service import UserService
from test_utils import MockResponse

# API Responses
OKAY_RESPONSE = MockResponse(200, {"OKAY"})
INTERNAL_SERVER_ERROR_RESPONSE = MockResponse(500, {"Error":"Internal Server Error"})

# Tests

# Validate that 'registerUserCardId' completes without exception
def test_registerUserCardId_returns_okay(mocker):
    mocker.patch('requests.put', return_value=OKAY_RESPONSE)
    userService = UserService()
    assert userService.registerUserCardId("1", "1234567890") is None

# Validate that 'registerUserCardId' throws 'Exception' when API responds with '500'
def test_registerUserCardId_returns_error(mocker):
    mocker.patch('requests.put', return_value=INTERNAL_SERVER_ERROR_RESPONSE)
    with pytest.raises(Exception) as excInfo:
        userService = UserService()
        userService.registerUserCardId("1", "1234567890")    
    assert "registerUserCardId API Error" in str(excInfo.value)

# Unit Tests for the Auth Service
# Needs update

import pytest
from context import stac_device
from auth_service import AuthService
from test_utils import MockResponse

# API Responses
OKAY_TRUE_RESPONSE = MockResponse(200, {"Authorized": True})
OKAY_FALSE_RESPONSE = MockResponse(200, {"Authorized":False})
INTERNAL_SERVER_ERROR_RESPONSE = MockResponse(500, {"Error":"Internal Server Error"})

# Tests

# Validate that 'checkToolAccess' returns 'True' when API responds with '200' and 'Authorized: True'
def test_checkToolAccess_returns_true(mocker):
    mocker.patch('requests.get', return_value=OKAY_TRUE_RESPONSE)

    authService = AuthService()
    assert authService.checkToolAccess("1234567890", 1)

# Validate that 'checkToolAccess' returns 'False' when API responds with '200' and 'Authorized: False'
def test_checkToolAccess_returns_false(mocker):
    mocker.patch('requests.get', return_value=OKAY_FALSE_RESPONSE)
    authService = AuthService()
    assert not authService.checkToolAccess("1234567890", 2)

# Validate that 'checkToolAccess' throws 'Exception' when API responds with '500'
def test_checkToolAccess_throws_exception(mocker):
    mocker.patch('requests.get', return_value=INTERNAL_SERVER_ERROR_RESPONSE)
    with pytest.raises(Exception) as excInfo:
        authService = AuthService()
        authService.checkToolAccess("1234567890", 3)    
    assert "checkToolAccess API Error" in str(excInfo.value)

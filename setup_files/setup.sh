# Setup script to install required libraries and enable necessary interfaces
# Sets STAC device script to run on boot-up

# Update Pi and Python
sudo apt-get update
sudo apt-get upgrade
sudo pip3 install --upgrade setuptools

# Install CircuitPython Libraries
cd ~
sudo pip3 install --upgrade adafruit-python-shell
wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/raspi-blinka.py
sudo python3 raspi-blinka.py

# Install library for Pi OLED
sudo pip3 install adafruit-circuitpython-ssd1306

# Install support Library for OLED: PIL allows text with custom fonts
sudo apt-get install python3-pil

# Install library for Matrix Keypad
sudo pip3 install adafruit-circuitpython-matrixkeypad

# Enable SPI
echo "dtparam=spi=on" >> /boot/config.txt

# Install Python tools
sudo apt-get install python3-dev python3-pip
sudo pip3 install spidev

# Install Libraries required for STAC device Python script
cd stac-device
sudo pip3 install -r requirements.txt

# Setup script to run on boot
mkdir /home/pi/.config/autostart
touch /home/pi/.config/autostart/StacDevice.desktop
echo "[Desktop Entry]" >> /home/pi/.config/autostart/StacDevice.desktop
echo "Type=Application" >> /home/pi/.config/autostart/StacDevice.desktop
echo "Name=StacDevice" >> /home/pi/.config/autostart/StacDevice.desktop
echo "Exec=/usr/bin/python3 /home/pi/stac-device/stac_device/main.py" >> /home/pi/.config/autostart/StacDevice.desktop

# Shutdown to apply settings
sudo shutdown -h now